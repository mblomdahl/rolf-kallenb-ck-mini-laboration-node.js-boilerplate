/**
 * request_handler.js
 */

function foo() {
    console.log("Request handler foo was called.");
    return "Hello Foo";
}

function bar() {
    console.log("Request handler bar was called.");
    return "Hello Bar";
}

exports.foo = foo;
exports.bar = bar;