/*router*/

function route(request, response) {
	var url = require("url");
    var pathname = url.parse(request.url).pathname;
    console.log("Request for " + pathname + " received.");
	
	var request_handler = require("./request_handler");
	var out;
	if(pathname == "/foo") {
		out = request_handler.foo();
	}
	else if(pathname == "/bar") {
		out = request_handler.bar();
	}
	else {
		out = "It's no good";
	}
	
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.write(out);
	response.end();
}

exports.route = route;