/*server*/

function start() {
	var http = require('http');
	var router = require("./router");
	http.createServer(router.route).listen(1337, '127.0.0.1');
	
	console.log('Server running at http://127.0.0.1:1337/');
}

exports.start = start;